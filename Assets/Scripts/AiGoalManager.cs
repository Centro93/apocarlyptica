﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiGoalManager : MonoBehaviour
{
    GameObject[] npcs;
    Destination[] destinations;

    void Start()
    {
        npcs = GameObject.FindGameObjectsWithTag("NPC");
        destinations = FindObjectsOfType<Destination>();

        foreach (GameObject npc in npcs)
        {
            npc.GetComponent<NPCController>().SetTarget(GetRandomDestination());
        }
    }

    public Vector3 GetRandomDestination(Destination excludedDestination = null)
    {
        Destination destination;
        do
        {
            destination = destinations[Random.Range(0, destinations.Length)];
        } while (destination == excludedDestination);
        return destination.gameObject.transform.Find("Waypoint").transform.position;
    }
}