﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitDistanceToParent : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject parentObject;
    public float maxDistance = 3f;
    public Vector3 previousPosition = new Vector3(0, 0, 0);
    public bool debug = false;
    private Rigidbody rb;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        previousPosition = transform.position;
        rb = parentObject.GetComponent<Rigidbody>();
        velocity = rb.velocity;
        if(debug) DebugGUI.SetGraphProperties("movementDelta", "movementDelta", -0.1f, 0.1f, 2, new Color(1, 0.3f, 0.5f), true);
    }

    // Update is called once per frame
    void Update()
    {
        previousPosition = transform.position;
        Vector3 targetVector = transform.position - parentObject.transform.position;

        // limit distance
        Vector3 movementVector = Vector3.Max(Vector3.Min(targetVector, targetVector.normalized * maxDistance), -targetVector.normalized * maxDistance);

        // smooth and apply movement
        movementVector = Vector3.SmoothDamp(previousPosition, parentObject.transform.position + movementVector, ref velocity, Time.deltaTime);
        transform.position = movementVector;

        if (debug) DebugGUI.Graph("movementDelta", (transform.position - previousPosition).magnitude);
    }
}
