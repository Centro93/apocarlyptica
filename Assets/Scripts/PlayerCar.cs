﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCar : MonoBehaviour
{
    [Tooltip("Set this Vector for changing the Car's Center of Mass.")]
    public Vector3 CenterOfMass;

    Rigidbody playerCarRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        playerCarRigidbody = GetComponentInChildren<Rigidbody>();
        playerCarRigidbody.centerOfMass += CenterOfMass;
    }

    public float GetCombinedVelocity()
    {
        float currentVelocity = Vector3.Magnitude(playerCarRigidbody.velocity);
        return currentVelocity;
    }
}
