﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject canvasDriving;
    public GameObject canvasDestination;
    public GameObject destinationButton;

    // Start is called before the first frame update
    void Start()
    {
        canvasDriving = GameObject.Find("Canvas Driving");
        canvasDestination = GameObject.Find("Canvas Destination");
        destinationButton = GameObject.Find("Destination Button");
        ToggleUiElement(canvasDestination);
        ToggleUiElement(destinationButton);
    }

    public void ToggleUiElement(GameObject uiElement)
    {
        if (uiElement.activeSelf)
        {
            uiElement.SetActive(false);
        }
        else
        {
            uiElement.SetActive(true);
        }
    }
}