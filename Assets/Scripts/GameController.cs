﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameObject Player;
    private Vector3 PlayerStartPosition;
    private Quaternion PlayerStartRotation;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        PlayerStartPosition = Player.transform.position;
        PlayerStartRotation = Player.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            resetPlayerPosition();
        }
    }

    private void resetPlayerPosition()
    {
        Debug.Log("Resetting Player to Start Position");
        Player.transform.position = PlayerStartPosition;
        Player.transform.rotation = Player.transform.rotation;
    }
}
