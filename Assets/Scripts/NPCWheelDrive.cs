﻿using UnityEngine;
using System;
using UnityEngine.AI;

public class NPCWheelDrive : MonoBehaviour
{
    private WheelCollider[] m_Wheels;
    private Rigidbody carRb;
    private float previousAngle = 0f;
    private NPCController npcController;

    void Awake()
    {
        npcController = transform.parent.parent.parent.GetComponent<NPCController>();
        m_Wheels = GetComponentsInChildren<WheelCollider>();
        carRb = npcController.CarBody.GetComponent<Rigidbody>();
    }

    void Start()
	{
        for (int i = 0; i < m_Wheels.Length; ++i) 
		{
			var wheel = m_Wheels [i];

			// Create wheel shapes only when needed.
			if (npcController.wheelShape != null)
			{
				var ws = Instantiate (npcController.wheelShape);
				ws.transform.parent = wheel.transform;
			}
        }
        if(npcController.debug && gameObject.name == "FrontLeftWheel")
        {
            DebugGUI.SetGraphProperties("velocity", "Velocity", -1, 1, 1, new Color(1, 1, 0), true);
            DebugGUI.SetGraphProperties("angleLerp", "Velocity Smoothing", -1, 1, 1, new Color(1, 1, 0), true);
            DebugGUI.SetGraphProperties("torque", "Torque", -1, 1, 1, new Color(1, 0, 0), true);
            DebugGUI.SetGraphProperties("braking", "Brake Torque", -1, 1, 1, new Color(1, 0, 1), true);
            DebugGUI.SetGraphProperties("targetDistance", "Target Distance", -1, 1, 1, new Color(1, 0.5f, 0), true);
            DebugGUI.SetGraphProperties("targetAngle", "Target Angle", -1, 1, 1, new Color(0.5f, 0.5f, 0), true);
            DebugGUI.SetGraphProperties("angle", "Angle", -1, 1, 1, new Color(1, 1, 1), true);
            DebugGUI.SetGraphProperties("velocityLimitedMaxAngle", "Max Angle", -1, 1, 1, new Color(0, 0.5f, 0.7f), true);
        }
	}

	public void MoveToPosition(Vector3 targetVelocity)
    {
        float currentVelocity = carRb.velocity.magnitude;
        float targetDistance = Vector3.Magnitude(targetVelocity);
        float targetAngle = Vector3.SignedAngle(targetVelocity, npcController.CarBody.transform.forward, Vector3.up);

        // speed up until limit is hit
        float torque = currentVelocity > npcController.maxVelocity ? 0 : Mathf.Clamp(targetVelocity.magnitude * npcController.torqueMultiplier, -npcController.maxTorque, npcController.maxTorque); // npcController.maxVelocity ? Mathf.Min(npcController.maxTorque, Mathf.Pow(targetDistance, 2) / velocity * npcController.velocityMultiplier) : 0f;

        // minmax angle
        float velocityLimitedMaxAngle = npcController.maxAngle - currentVelocity;
        float angle = -Mathf.Clamp(targetAngle, -velocityLimitedMaxAngle, velocityLimitedMaxAngle);

        // smooth angle 
        float angleLerp = 5 / targetDistance * Time.deltaTime;
        angle = Mathf.Lerp(angle, previousAngle, angleLerp);


        // brake if AI Agent comes close
        float braking = targetDistance < 1f ? npcController.brakeTorque : 0;

        // Reverse if target is behind car
        if (Mathf.Abs(targetAngle) >= 90)
        {
            torque = -torque;
            angle = -angle;
        }

        // cache current angle for lerping
        previousAngle = angle;

        // debug stuff
        if (npcController.debug && gameObject.name == "FrontLeftWheel")
        {
            DebugGUI.Graph("angleLerp", angleLerp);
            DebugGUI.Graph("velocity", currentVelocity);
            DebugGUI.Graph("targetDistance", targetDistance);
            DebugGUI.Graph("velocityLimitedMaxAngle", velocityLimitedMaxAngle);
            DebugGUI.Graph("targetAngle",  targetAngle);
            DebugGUI.Graph("angle", angle);
            DebugGUI.Graph("torque", torque);
            DebugGUI.Graph("braking", braking);
        }


        // This is a really simple approach to updating wheels.
        // We simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero.
        // This helps us to figure our which wheels are front ones and which are rear.
        foreach (WheelCollider wheel in m_Wheels)
		{
			// A simple car where front wheels steer while rear ones drive.
			if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;

			if (wheel.transform.localPosition.z < 0)
			{
				wheel.brakeTorque = braking;
			}

			if (wheel.transform.localPosition.z < 0 && npcController.driveType != DriveType.FrontWheelDrive)
			{
				wheel.motorTorque = torque;
			}

			if (wheel.transform.localPosition.z >= 0 && npcController.driveType != DriveType.RearWheelDrive)
			{
				wheel.motorTorque = torque;
			}

			// Update visual wheels if any.
			if (npcController.wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// Assume that the only child of the wheelcollider is the wheel shape.
				Transform shapeTransform = wheel.transform.GetChild (0);

                shapeTransform.rotation = q * Quaternion.Euler(0, 0, -90);
                shapeTransform.position = p;
                
			}
		}
	}
}
