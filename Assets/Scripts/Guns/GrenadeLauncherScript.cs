﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncherScript : MonoBehaviour
{
    [Tooltip("If true, the player controls this gun. Else, it is controlled by AI.")]
    public bool playerControlled = false;
    [Tooltip("The speed at which the gun turns.")]
    public float turretSpeed = 5f;
    [Tooltip("The speed at which the gun fires in seconds.")]
    public float timeBetweenShots = 0.45f;
    [Tooltip("The Munition to shoot")]
    public GameObject Munition;
    [Tooltip("The lowest possible aiming-angle of the turret in degrees.")]
    public float minAimingAngle = -3f;
    [Tooltip("The highest possible aiming-angle of the turret in degrees.")]
    public float maxAimingAngle = 20f;

    private GameObject CarBody;
    private Transform Nozzle;
    private Camera camera;

    private Vector3 aimZeroingVector = new Vector3(0, 1f, 0);
    private float lastFiredTimestamp = -100000f;


    // Start is called before the first frame update
    void Awake()
    {
        CarBody = transform.root.Find("CarBody").gameObject;
        Nozzle = transform.Find("Nozzle");
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playerControlled) usePlayerControls();
        else useAiControls();
    }

    void usePlayerControls()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if(hit.transform.root.name != "PlayerCar")
            {
                aimTurret(hit.point + aimZeroingVector);
            }
        }

        if (Input.GetMouseButton(0))
        {
            Fire();
        }
    }

    void useAiControls()
    {

    }

    void aimTurret(Vector3 target)
    {
        Quaternion targetRotation = Quaternion.LookRotation(target - transform.position);
        // smooth rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turretSpeed * Time.deltaTime);
        // apply rotation limits
        Vector3 localRotationEuler = transform.localRotation.eulerAngles;
        float limitedXAngle = Mathf.Clamp(localRotationEuler.x, -maxAimingAngle, -minAimingAngle);
        localRotationEuler.x = limitedXAngle;
        transform.localRotation = Quaternion.Euler(localRotationEuler);
    }

    void Fire()
    {
        if(Time.time - lastFiredTimestamp >= timeBetweenShots)
        {
            Instantiate(Munition, Nozzle.position, Nozzle.rotation);
            lastFiredTimestamp = Time.time;
        }
    }
}
