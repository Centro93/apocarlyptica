﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncherShellScript : MonoBehaviour
{
    [Tooltip("The Force applied to the shell on firing.")]
    public float firingForce = 1000f;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        
        rb.AddForce(transform.forward * firingForce, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Explode();
    }

    private void Explode()
    {
        Destroy(gameObject);
    }
}
