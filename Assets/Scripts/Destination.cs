﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Destination : MonoBehaviour
{
    MenuManager menuManager;
    AiGoalManager aiGoalManager;

    private void Start()
    {
        menuManager = FindObjectOfType<MenuManager>();
        aiGoalManager = FindObjectOfType<AiGoalManager>();
    }

    private void OnTriggerEnter(Collider visitor)
    {
        if (visitor.transform.parent.GetComponent<NPCController>() != null)
        {
            visitor.transform.parent.GetComponent<NPCController>().SetTarget(aiGoalManager.GetRandomDestination(gameObject.GetComponent<Destination>()));
        }
        else if (visitor.transform.parent.GetComponent<PlayerCar>() != null)
        {
            menuManager.ToggleUiElement(menuManager.destinationButton);
        }
    }

    private void OnTriggerExit (Collider visitor)
    {
        if (visitor.transform.parent.GetComponent<PlayerCar>() != null)
        {
            menuManager.ToggleUiElement(menuManager.destinationButton);
        }

    }
}