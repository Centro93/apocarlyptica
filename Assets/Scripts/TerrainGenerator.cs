﻿using System;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public int depth = 3;

    public int width = 256;
    public int heigth = 256;

    public float scale = 5.0f;

    private void Start()
    {
        Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }

    private TerrainData GenerateTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = width + 1;

        terrainData.size = new Vector3(width, depth, heigth);

        terrainData.SetHeights(0, 0, GenerateHeights());
        return terrainData;
    }

    private float[,] GenerateHeights()
    {
        float[,] heights = new float[width, heigth];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < heigth; y++)
            {
                heights[x, y] = CalculateHeight(x, y);
            }
        }

        return heights;
    }

    private float CalculateHeight (int x, int y)
    {
        float xCoord = (float)x / width * scale;
        float yCoord = (float)y / heigth * scale;

        return Mathf.PerlinNoise(xCoord, yCoord);
    }
}
