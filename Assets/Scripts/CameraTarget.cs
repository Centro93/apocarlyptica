﻿using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    [SerializeField] GameObject cameraTarget;

    void Update()
    {
        if (cameraTarget)
        {
            gameObject.transform.position = cameraTarget.transform.position;
        }
    }
}