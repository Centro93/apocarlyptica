﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedoHand : MonoBehaviour
{
    PlayerCar playerCar;
    RectTransform hand;

    // Start is called before the first frame update
    void Start()
    {
        playerCar = FindObjectOfType<PlayerCar>() ;
        hand = gameObject.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float newHandRotationZ = playerCar.GetCombinedVelocity() * -0.02f;
        Quaternion newHandRotation = new Quaternion(hand.rotation.x, hand.rotation.y, newHandRotationZ, hand.rotation.w);
        hand.rotation = newHandRotation;
    }
}