﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[System.Serializable]
public class TargetMovedEvent : UnityEvent<Vector3>
{
}

public class FollowAIAgent : MonoBehaviour
{
    public TargetMovedEvent targetMovedEvent;
    public bool debug = false;
    private NavMeshAgent carAgent;
    private NavMeshAgent scoutAgent;
    private NPCController npcController;

    // Start is called before the first frame update
    void Awake()
    {
        if (targetMovedEvent == null)
            targetMovedEvent = new TargetMovedEvent();

        npcController = transform.parent.GetComponent<NPCController>();
        GameObject AiScout = npcController.CarBody.transform.parent.Find("AI Scout").gameObject;
        scoutAgent = AiScout.GetComponent<NavMeshAgent>();
        carAgent = transform.parent.Find("AI Agent").GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 carAgentVelocity = carAgent.desiredVelocity;
        Vector3 scoutAgentVelocity = scoutAgent.desiredVelocity;
        Vector3 scoutAgentRelativePositionToCar = scoutAgent.transform.position - npcController.CarBody.transform.position;

        // If the two AI Agents disagree, follow AI Scouts angle and velocity more closely
        float angleDelta = Vector3.Angle(carAgentVelocity, scoutAgentVelocity);
        Vector3 travelVector = Vector3.Lerp(carAgentVelocity, scoutAgentVelocity, Time.deltaTime * angleDelta);

        travelVector = travelVector * (1 / npcController.aiAgentMaxDistance * scoutAgentRelativePositionToCar.magnitude);

        // inform wheels of the new travel vector
        targetMovedEvent.Invoke(travelVector);

        if(debug)
        {
            Debug.DrawLine(carAgent.transform.position, carAgent.transform.position + carAgentVelocity, new Color(255, 0, 0));
            Debug.DrawLine(carAgent.transform.position, carAgent.transform.position + travelVector, new Color(0, 255, 0));
            Debug.DrawLine(scoutAgent.transform.position, scoutAgent.transform.position + scoutAgentVelocity, new Color(0, 0, 255));
        }
    }
}
