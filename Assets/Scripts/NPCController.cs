﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    [Tooltip("The maximum allowed distance of the AI Agent")]
    public float aiAgentMaxDistance = 3f;
    [Tooltip("The maximum allowed speed of the AI Agent")]
    public float aiAgentMaxSpeed = 10f;
    [Tooltip("The acceleration of the AI Agent")]
    public float aiAgentAcceleration = 8f;
    [Tooltip("Maximum steering angle of the wheels")]
    public float maxAngle = 30f;
    [Tooltip("Maximum torque applied to the driving wheels")]
    public float maxTorque = 200f;
    [Tooltip("Maximum velocity")]
    public float maxVelocity = 10f;
    [Tooltip("Torque Multiplier / Acceleration")]
    public float torqueMultiplier = 150f;
    [Tooltip("Maximum brake torque applied to the driving wheels")]
    public float brakeTorque = 30000f;
    [Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
    public GameObject wheelShape;
    [Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
    public DriveType driveType;
    [Tooltip("Set this Vector for changing the Car's Center of Mass.")]
    public Vector3 CenterOfMass;
    [Tooltip("The Body of the Car.")]
    public GameObject CarBody;

    [Tooltip("Set an Gameobject for an initial Ai Agent Target.")]
    public GameObject initialTarget;
    [Tooltip("Enable Logs")]
    public bool debug = false;

    private GameObject ScoutAgentGo;
    private GameObject CarAgentGo;
    private NavMeshAgent carAgent;
    private NavMeshAgent scoutAgent;
    private FollowAIAgent followAIAgent;
    private LimitDistanceToParent limiter;
    private Rigidbody carRigidbody;

    // Start is called before the first frame update
    void Awake()
    {
        CarBody = transform.Find("CarBody").gameObject;
        carRigidbody = CarBody.GetComponent<Rigidbody>();

        ScoutAgentGo = transform.Find("AI Scout").gameObject;
        scoutAgent = ScoutAgentGo.GetComponent<NavMeshAgent>();

        CarAgentGo = transform.Find("AI Agent").gameObject;
        carAgent = CarAgentGo.GetComponent<NavMeshAgent>();

        followAIAgent = CarBody.GetComponent<FollowAIAgent>();
        limiter = ScoutAgentGo.GetComponent<LimitDistanceToParent>();
    }

    void Start()
    {
        carAgent.speed = aiAgentMaxSpeed;
        scoutAgent.speed = aiAgentMaxSpeed;
        carAgent.acceleration = aiAgentAcceleration;
        scoutAgent.acceleration = aiAgentAcceleration;

        limiter.maxDistance = aiAgentMaxDistance;
        
        if (debug)
        {
            limiter.debug = true;
            followAIAgent.debug = true;
        }

        carRigidbody.centerOfMass += CenterOfMass;

        if(initialTarget)
        {
            SetTarget(initialTarget.transform.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTarget(Vector3 target)
    {
        carAgent.destination = target;
        scoutAgent.destination = target;
    }
}
